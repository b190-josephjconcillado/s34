// npm init
/**
 * triggering this command will prompt the user for different settings that will define the application 
 * using this command will make a "package.json"
 */
/**
 * npm install express
 *  after triggering this command, express will now be listed as a dependency. this can be seen inside the "package.json" file under the "dependencies"
 */

const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.listen(port,()=>console.log(`Server running at port: ${port}`));
//SECTION - ROUTES
// GET method
app.get("/",(req,res)=>{
    res.send("Hello World");
});

app.get("/hello",(req,res)=>{
    res.send("Hello from /hello endpoint");
});

// POST method
app.post("/hello",(req,res) =>{
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [];
app.post("/signup",(req,res)=>{
    if(req.body.firstName !== "" && req.body.lastName !== "") {
        // users = {
        //     "firstName": req.body.firstName,
        //     "lastName": req.body.lastName
        // }
        users.push(req.body);
        console.log(users);
        res.send(`User ${req.body.firstName} ${req.body.lastName} has signed up successfully!`)
    }else{
        res.send("firstName and lastName should not be empty.")
    }
});

app.put("/change-lastName",(req,res)=>{
    let message;
    for(let i=0; i<users.length;i++){
        if(req.body.firstName == users[i].firstName){
            users[i].lastName = req.body.lastName;
            message = `User ${users[i].firstName} has successfully changed lastName into ${req.body.lastName}`;
            break;
        }else{
            message = "User does not exist";
        }
    }
    console.log(users);
    res.send(message);
});

/**
 * SECTION - ACTIVITY
 */
// Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home",(req,res)=>{
    res.send("Welcome to the home page");
});

// Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
app.get("/users",(req,res)=>{
    res.send(users);
});
// Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/users",(req,res)=>{
    let message;
    for(let i=0; i<users.length;i++){
        if(users[i].firstName === req.body.firstName || users[i].lastName === req.body.lastName){
            message = `User ${users[i].firstName} ${users[i].lastName} has been deleted`;
            users.pop(i);
            break;
        }else{
            message = "User does not exist!"
        }
    }
    if(users.length === 0){
        message = "User does not exist!";
    }
    console.log(users);
    res.send(message);
});